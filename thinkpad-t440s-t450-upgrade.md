## Requirements:
- Lenovo Thinkpad T440s drivers for Windows 10 | [Download](https://download.lenovo.com/pccbbs/mobiles/gggr07ww.exe) | [Info](https://download.lenovo.com/pccbbs/mobiles/gggr07ww.txt)
- Lenovo Thinkpad T450 drivers for Windows 8.1/7 | [Download](https://download.lenovo.com/pccbbs/mobiles/jbgc28ww.exe) | [Info](https://download.lenovo.com/pccbbs/mobiles/jbgc28ww.txt)
- Lenovo Thinkpad T570 drivers for Windows 10 | [Download](https://download.lenovo.com/pccbbs/mobiles/n1mgx14w.zip) | [Info](https://download.lenovo.com/pccbbs/mobiles/n1mgx14w.txt)

## Tutorial:

- Turn off your internet connection so Windows Update won't get in your way
- Find your `Synaptics Pointing Device` in `Device Manager > Mice and other pointing devices`
- Right click on it and find its ID in `Properties > Details > Hardware Ids`
- Right click and copy the value at the very top (in my case `ACPI\VEN_LEN&DEV_0036`)
- Save it in a text file somewhere on your Desktop as you'll need it at the end
- Right click on `Synaptics Pointing Device`, uninstall it and restart your system
---
- Run the T440s drivers installer, let the process finish and restart your system
- `Start > Mouse Properties (Touchpad Clickpad Trackpad TrackPoint Mouse Pointer Pointing Pad)`
- Disable top and bottom buttons in `Touchpad` and gestures in `Gestures`
- Click on `Advanced` in the top right corner
- Disable each and every option inside and apply your changes
- Run the T450 drivers installer <ins>**BUT**</ins> remember to toggle off the check at the end
- Find `SynPD.inf` in `C:\DRIVERS\WIN\UltraNav\WinWDF\x64` and open it with a text editor
    - Find and delete all the lines containing `LEN0036`
    - Find all the lines containing `LEN200E` and replace the ID with `LEN0036`
- Save the changes and while keeping the `Shift` key pressed, restart your system
- In the menu navigate through `Troubleshoot > Advanced options > Startup settings`
- Click on `Restart` and once prompted press `7` (aka `Disable driver signature enforcement`)
---
- Find `Synaptics Pointing Device` in `Device Manager > Mice and other pointing devices`
- Right click on it, click on `Update driver` and:
	1. `Browse my computer for drivers`
	2. `Let me pick from a list of available drivers on my computer`
	3. `Have disk... > Browse...` 
    4. Pick `SynPD.inf` we just edited
	5. `Next > Install this driver software anyway`
- Restart your system one more time to apply changes and to re-enable driver signature enforcement
---
- Extract the T570 drivers from their zip archive
- Find `Synaptics Pointing Device` in `Device Manager > Mice and other pointing devices`
- Right click on it, click on `Update driver` and:
	1. `Browse my computer for drivers`
	2. `Let me pick from a list of available drivers on my computer`
	3. `Have disk... > Browse...` 
    4. Pick `SynPD.inf` from the archive we just extracted
    5. Pick `Synaptics Pointing Device` ( <ins>**NOT**</ins> `ThinkPad Pointing Device` )
- Restart your system for the last time to apply the changes
---
Verify that the precision gestures are now available in `Start > Settings > Devices > Touchpad`.  

If everything went correctly you should now be able to use:
- the physical buttons at the top from the T450 touchpad
- tap to click feature (one, two and three fingers taps)
- precision gestures from the Windows Settings menu

If <ins>**ANY**</ins> of the three isn't working you fucked up somewhere and have to start from top again..

Now that you hopefully got everything working you have to make sure Windows Updates won't overwrite your custom drivers:
1. Find the text file you saved at the beginning with your hardware ID and copy its content
2. Open your Group Policies with `Start > Edit group policy` and navigate through:
    1. `Computer Configuration > Administrative Templates`
    2. `System > Device Installation > Device Installation Restrictions`
    3. `Prevent installation of devices that match any of these device IDs`
3. Enable the policy, click on `Show...` and add to the list the id you just copied

You can now re-enable your networking and enjoy your extra buttons + precision gestures!

## Post install (optional):

- Make the physical middle button act as one instead of a glorified scroll wheel:  
    `Start > Mouse Properties (Touchpad Clickpad Trackpad TrackPoint Mouse Pointer Pointing Pad)`.
- Tweak the two fingers scroll speed by lowering the value (`1` is just nice) of this regedit key (Lower is better.):  
    `Computer\HKEY_CURRENT_USER\Software\Microsoft\Wisp\Touch\Friction`

## Documentation used:
- https://web.archive.org/web/20190301194611/https://forum.thinkpads.com/viewtopic.php?p=822927
- https://web.archive.org/web/20230323142259/https://old.reddit.com/r/thinkpad/comments/dwihtt/thinkpad_precision_drivers_guide_t450s_and_other
- https://web.archive.org/web/20230314183450/https://www.tenforums.com/tutorials/156602-how-enable-disable-driver-signature-enforcement-windows-10-a.html
- https://web.archive.org/web/20220825150056/https://superuser.com/questions/1209746/increase-precision-touchpad-two-finger-scrolling-speed
