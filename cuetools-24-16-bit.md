CUETools can only[^1] process `16 bit / 44.1 kHz` and if your files are `24 bits` per sample instead of the standard `16` you'll have to convert them before splitting.

1. Download foobar2000, its [Free Encoder Pack](https://www.foobar2000.org/download) and [CUETools](http://cue.tools/wiki/CUETools_Download).
2. Add the FLAC track to a foobar2000 playlist, `Edit` > `Select All`, right click, `Convert` > `...`
3. Choose FLAC as output format and remember to flag the output bit depth to 16 bit instead of 24 bit.
4. Move to the `Processing` window, add the `Resampler DSP` and configure it to `44.1 kHz`.
5. Use a different path as output destination (ex: make a new folder in the source folder) and click on `Convert`.
6. Make a copy of the original `.cue` file and paste it in the folder you just created.
7. Open CUETools, drag and drop the `.cue` file into the `Input` bar, flag `Action > Encode` and `Mode > Tracks`.
8. Move to CUETools settings, check in the `HDCD` tab and toggle off `Decode HDCD to 20 bit`.
9. Click on `GO` and follow the wizard guidelines to correctly import metadata and track names.

[^1]: https://web.archive.org/web/20170806031040/http://www.digital-inn.de/threads/audio-24-bit-ripping-in-flac-eac.47397/