1. Download[^1] and burn[^2] the Raspberry Pi OS Lite ISO on a compatible SD card[^3]
2. Enable Wi-Fi and SSH headlessly[^4], eject the SD, boot your Pi and let it run for 1-2 minutes
3. Login via SSH[^5] from another terminal, update your system[^6] and start the config wizard[^7]
4. Change the default password, set `CLI (Autologin)` as boot method and set your own timezone in `Localization`
5. Install Transmission-daemon, Samba, Flexget and its dependencies
    ```bash
    sudo apt install transmission-daemon samba samba-common-bin smbclient cifs-utils python3-venv
    python3 -m venv ~/flexget/ && cd ~/flexget
    bin/pip install wheel && bin/pip install flexget transmission-rpc
    cd ; echo 'alias flexget="$HOME/flexget/bin/flexget"' >> ~/.bash_aliases && source ~/.bashrc
    ```
6. Change transmission-daemon's service user to your own and let it create some initial config files
    ```bash
    sudo systemctl stop transmission-daemon
    sudo systemctl edit transmission-daemon
    ```
    ```ini
    # Enter the following custom configuration
    [Service]
    User=pi
    ```
    ```bash
    sudo systemctl start transmission-daemon && sudo systemctl stop transmission-daemon
    ```
7. Add at least the bare minimum config[^8] to Transmission and finally start its service
    ```bash
    nano ~/.config/transmission-daemon/settings.json
    ```
    ```json
    "download-dir": "/home/pi/Downloads",
    "rpc-authentication-required": true,
    "rpc-username": "transmission",
    "rpc-password": "a-password-of-your-choice",
    "rpc-whitelist": "127.0.0.1,192.168.*.*",
    ```
    ```bash
    sudo systemctl start transmission-daemon
    ```
8. Configure[^9] and schedule Flexget with cron[^10], then initialize it
    ```bash
    mkdir -p ~/.config/flexget
    nano ~/.config/flexget/config.yml
    ```
    ```yaml
    tasks:
      anime:
        transmission:
          username: 'my-rpc-username'
          password: 'my-rpc-password'
          path: /home/pi/Downloads
        inputs:
          - rss: <feed url>
        series:
          - My anime title goes here
          - My other anime title as well
          - And so on!
    ```
    ```bash
    crontab -e
    ```
    ```
    @hourly /home/pi/flexget/bin/flexget --cron execute
    ```
    ```bash
    flexget execute
    ```
9. Give yourself SMB credentials, share your home folder and map it on your PC[^11]
    ```bash
    sudo smbpasswd -a pi
    sudo nano /etc/samba/smb.conf
    ```
    ```ini
    ; scroll at the bottom of the file and add the following lines
    [transmission]
    path = /home/pi/Downloads
    valid users = pi
    read only = no
    ```
    ```bash
    sudo systemctl restart {smbd,nmbd} 
    echo "This is the address for my samba share: \\\\$(hostname -i | xargs)\\transmission"
    ```

## FAQ

#### *How do I remove a series or episode from history?*
1. Manually run flexget to generate verbose logs with `flexget execute`
2. Check `~/.config/flexget/flexget.log`, find the rejected show/episode and take note of the plugin that caused it  
Ex: `REJECTED: "[Erai-raws] Kage no Jitsuryokusha ni Naritakute! - 02 [1080p] [ENG]" by series plugin because [..]`
3. Run `flexget <plugin-name> forget "<show-name>" [episode-number]` to remove the entry from the plugin's history

---


[^1]: https://www.raspberrypi.org/software/operating-systems/#raspberry-pi-os-32-bit
[^2]: https://www.raspberrypi.org/documentation/computers/getting-started.html#installing-images-on-windowsd
[^3]: https://elinux.org/RPi_SD_cards
[^4]: https://www.raspberrypi.org/documentation/computers/configuration.html#setting-up-a-headless-raspberry-pi
[^5]: https://www.raspberrypi.org/documentation/computers/remote-access.html#enabling-the-server
[^6]: https://www.raspberrypi.org/documentation/computers/os.html#keeping-your-operating-system-up-to-date
[^7]: https://www.raspberrypi.org/documentation/computers/configuration.html#raspi-config
[^8]: https://github.com/transmission/transmission/wiki/Editing-Configuration-Files#options
[^9]: https://flexget.com/Configuration#step-by-step-configuration-tutorial
[^10]: https://www.raspberrypi.org/documentation/computers/using_linux.html#scheduling-tasks-with-cron
[^11]: https://support.microsoft.com/en-us/windows/map-a-network-drive-in-windows-10-29ce55d1-34e3-a7e2-4801-131475f9557d