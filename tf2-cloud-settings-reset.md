0. **[Optional]** Backup your custom folder somewhere safe  
   `C:\Program Files (x86)\Steam\steamapps\common\Team Fortress 2\tf\custom\your_custom_folder\`
1. Right click on TF2 in your Steam library > `Properties...` > Disable `Steam Cloud`.
2. Move to `Local Files > Browse...` and delete all the files in  
   `C:\Program Files (x86)\Steam\steamapps\common\Team Fortress 2\tf\custom\`  
   `C:\Program Files (x86)\Steam\steamapps\common\Team Fortress 2\tf\cfg\`
3. Now click on `Local Files > Verify integrity of game files...` to download new default files.
4. Move back to `General` tab in the left side menu and set the [following](https://docs.mastercomfig.com/latest/setup/clean_up/) as `Launch Options`  
   `-novid -windowed -autoconfig -default +host_writeconfig config.cfg full +mat_savechanges +quit`
5. Launch the game, wait for it to auto-quit and remove the previously added launch options.
6. Exit steam, open a powershell window and run
   ```powershell
   cd "C:\Program Files (x86)\Steam\userdata\YOUR-USER-ID\440"
   ```
7. Copy-paste the following one-liner to overwrite the content of the `remote` directory  
   ```powershell
   # Remove -WhatIf flag to actually run the command
   ForEach($file in dir -Recurse -Attributes !Directory -Path ".\remote") {clc -WhatIf -Confirm $file.FullName}
   ```
8. Delete `remotecache.vdf` in `C:\Program Files (x86)\Steam\userdata\YOUR-ID\440\`
9. Start Steam, re-enable `Steam Cloud` for TF2 and click on `File Conflict` under `CLOUD STATUS`.
10. Select `Local Save` and wait for Steam to overwrite the files in the Cloud using your Local ones.

# Credits
- https://archive.is/JowGy
- https://archive.is/PMYyl
- https://archive.is/Nc6Ym#uninstall
