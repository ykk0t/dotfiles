1. Create a new directory somewhere safe
2. Create two text files and change their extension to `.bat` and `.xml`
3. Install [AHK](https://www.autohotkey.com/) if you haven't
4. Create a new AHK script in the directory just created and add the following lines:
   ```ahk
   #Requires AutoHotkey v2.0
   
   bat_name := "Name of your script.bat"
   bat_path := "C:\path\to\your\file.bat"
   
   Run "steam://rungameid/440"
   
   if WinWait("ahk_exe hl2.exe",, 60)
   	Run 'cmd.exe /c "' bat_name '"', bat_path, "Hide"
   else
   	ExitApp
   ```
4. Change `bat_name` and `bat_path` values accordingly and save the changes.
5. Open the `.bat` script with a text editor and copy-paste the following line:
   ```batch
   control intl.cpl,, /f:"%CD%\name of your file.xml"
   ```
6. Change the `.xml` file name as well in here and save the changes.
7. Open the `.xml` file with a text editor, finally copy-paste the following and save the changes:
   ```xml
   <gs:GlobalizationServices xmlns:gs="urn:longhornGlobalizationUnattend">
   
       <!--User List-->
       <gs:UserList>
           <gs:User UserID="Current"/>
       </gs:UserList>
   
       <!--input preferences--> 
       <gs:InputPreferences>
           <!--add en-US keyboard input-->
           <gs:InputLanguageID Action="add" ID="0409:00000409"/>
           <!--remove en-US keyboard input-->
           <gs:InputLanguageID Action="remove" ID="0409:00000409"/>
       </gs:InputPreferences>
   
   </gs:GlobalizationServices>
   ```

## Optional

- Right click on the `.ahk` script, click on `Create shortcut` and move it somewhere on your `Desktop` for easier access.
- Right click on the shortcut you just created, rename it if you wish so and click on `Change Icon...` in the `Shortcut` tab.
- Click on `Browse`, navigate to `..\Team Fortress 2\tf\resource\`, open `game.ico` and confirm to apply the changes.